# Detailed Pipeline Overview for `.gitlab-ci.yml`

## Pipeline Description

This GitLab CI/CD pipeline is specifically configured for the building, securing, and deployment processes of a MongoDB evaluation Docker image (`mongo-eval`). The pipeline integrates with Azure services and ensures thorough security checks.

## Pipeline Stages

### 1. Security Gates
- **Spectral Secret Gate:** Scans the codebase for secrets using SpectralOps tools.
- **Spectral IaC Gate:** Checks Infrastructure as Code (IaC) for security issues.

### 2. Build Image (`build_image`)
- Builds the MongoDB evaluation Docker image named `mongo-eval`.
- Handles the tagging and pushing of the image to the GitLab registry.

### 3. ShiftLeft Security Scan (`shiftleft_scan`)
- Retrieves and scans the Docker image from the GitLab registry using ShiftLeft to detect vulnerabilities.

### 4. Push to Azure Container Registry (ACR) (`push_to_acr`)
- Transfers the Docker image from the GitLab registry to Azure Container Registry.

### 5. ACR ShiftLeft Scan (`acr_shiftleft_scan`)
- Conducts an additional ShiftLeft scan of the Docker image within the Azure Container Registry for enhanced security.

### 6. Deploy to Azure Kubernetes Service (AKS) (`deploy_to_aks`)
- Deploys the secured Docker image to Azure Kubernetes Service.
- Utilizes tools like `kubectl` for the Kubernetes deployment configuration.

## Variables

- `IMAGE_NAME`: "mongo-eval" - The name of the Docker image.
- `IMAGE_TAG`: The tag for the Docker image in the GitLab registry.
- `AZURE_IMAGE_TAG`: The tag for the Docker image in the Azure Container Registry.

## Key Points

- Emphasis on Security: The pipeline prioritizes security scans at multiple stages.
- Azure Integration: Smoothly integrated with Azure Container Registry and Azure Kubernetes Service.
- Automated Deployment Process: Incorporates Docker and Kubernetes tools for a streamlined deployment workflow.

### Best Practices

- Regularly update the `.gitlab-ci.yml` file to reflect project changes.
- Securely handle credentials and sensitive information.
- Continuously monitor and refine the pipeline for optimal performance.
